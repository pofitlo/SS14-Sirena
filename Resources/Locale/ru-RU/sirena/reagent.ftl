reagent-name-artecalcet = артекальцет
reagent-desc-artecalcet = Препарат на основе серебра. Используется во всех наркотиках, изменяющих сознание. В чистом виде вызывает отравление.

reagent-name-arteicog = артеиког
reagent-desc-arteicog = Препарат на основе артекальцета. При воздействии на разумного человека вызывает седативный эффект. При передозировке усыпляет цель.
arteicog-effect-1 = Вы чувствуете спокойствие
arteicog-effect-2 = Ваши мышцы расслаблены
arteicog-effect-3 = Вы чувствуете себя безмятежно
arteicog-effect-4 = Ваш разум настолько расслаблен, что вы спите
arteicog-effect-5 = Вы чувствуете, что ваши конечности слишком тяжелые
arteicog-effect-6 = Вы комфортно отдыхаете, ни о чем не беспокоясь

reagent-name-gadoiprinim = гадоиприним
reagent-desc-gadoiprinim = Препарат на основе артекальцета. Уменьшает болевые ощущения при введении внутрь.
gadoiprinim-effect-1 = Ваше тело чувствует меньше боли

reagent-name-vinikefastatin = виникефастатин
reagent-desc-vinikefastatin = Препарат на основе артекальцета. Препарат активизирующий некоторые отделы мозга.
vinikefastatin-effect-1 = Вы чувствуете бодрость 
vinikefastatin-effect-2 = В вашей голове пробегает множество мыслей
vinikefastatin-effect-3 = Все не кажется таким уж и безнадежным
vinikefastatin-effect-4 = Вас наполняет решимость
vinikefastatin-effect-5 = Ваш разум наполняется уверенностью

reagent-name-aphrodisiac = афродизиак
reagent-desc-aphrodisiac = Препарат на основе артекальцета. Стимулирует сексуальное возбуждение.
aphrodisiac-effect-1 = Вы чувствуете слабое сексуальное влечение к ближайшим разумным.
aphrodisiac-effect-2 = Ваш разум заполняется мыслями о сексе.
aphrodisiac-effect-3 = Ваши ноги начинают слегка подкашиваться.
aphrodisiac-effect-4 = Вы чувствуете лёгкий дискомфорт снизу.
aphrodisiac-effect-5 = Вы чувствуете сексуальное влечение к ближайшим разумным.
aphrodisiac-effect-6 = Ваш разум заполняется мыслями о сексе.
aphrodisiac-effect-7 = Ваши ноги чуть подкашиваются.
aphrodisiac-effect-8 = Вы чувствуете дискомфорт снизу.
aphrodisiac-effect-9 = Вы чувствуете хотите разумных рядом с собой.
aphrodisiac-effect-10 = Ваш разум заполняется только мыслями о сексе.
aphrodisiac-effect-11 = Ваши ноги иногда подкашиваются.
aphrodisiac-effect-12 = Снизу всё зудит и вам хочется поскорее удовлетворить себя.
aphrodisiac-effect-13 = ВЫ ХОТИТЕ РЯДОМ СТОЯЩЕГО РАЗУМНОГО!
aphrodisiac-effect-14 = ВЫ НЕ МОЖЕТЕ НИ О ЧЁМ ДУМАТЬ КРОМЕ СЕКСА!
aphrodisiac-effect-15 = Ваши ноги не держат вас!
aphrodisiac-effect-16 = Изо рта разумного обильно течёт пена
aphrodisiac-effect-17 = Разумного трясёт
aphrodisiac-effect-18 = Половые органы разумного сильно возбуждены

reagent-physical-desc-silver = серебряный

reagent-name-bluespacecrystal = блюспей кристалл
reagent-desc-bluespacecrystal = Перемолотые блюспей кристаллы
reagent-physical-desc-bluespacecrystal = нестабильный

materials-bluespacecrystal = блюспейс кристалл