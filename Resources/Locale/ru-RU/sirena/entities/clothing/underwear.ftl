ent-ClothingUnderwearSocksHigh = высокие носки
    .desc = Высокие носки Nanotrasen для сотрудников станции. Покрашены в белый цвет.

ent-ClothingUnderwearSocksNormal = носки
    .desc = Стандартные носки Nanotrasen для сотрудников станции. Покрашены в белый цвет.

ent-ClothingUnderwearSocksShort = короткие носки
    .desc = Короткие носки Nanotrasen для сотрудников станции. Покрашены в белый цвет.

ent-ClothingUnderwearSocksThigh = чулки
    .desc = Стандартные чулки Nanotrasen для сотрудников станции. Покрашены в белый цвет.

ent-ClothingUnderwearBottomPantiesWhite = трусики
    .desc = Стандартное нижнее бельё Nanotrasen для сотрудниц станции. Покрашены в белый цвет.

ent-ClothingUnderwearBottomBoxersWhite = боксеры
    .desc = Стандартное нижнее бельё Nanotrasen для сотрудников станции. Покрашены в белый цвет.

ent-ClothingUnderwearTopBraWhite = бра
    .desc = Стандартный лифчик Nanotrasen для сотрудниц станции. Покрашен в белый цвет.

ent-ClothingUnderwearTopBraSports = спортивное бра
    .desc = Лифчик для занятий спортом. Покрашен в белый цвет.

ent-ClothingUnderwearTopBraSportsAlternative = альтернативное спортивное бра
    .desc = Лифчик для занятий спортом. Альтернативная версия. Покрашен в белый цвет.

ent-ClothingUnderwearBottomBoxersCap = боксеры капитана
    .desc = Стандартное нижнее бельё капитана станции.

ent-ClothingUnderwearBottomBoxersCE = боксеры СИ
    .desc = Стандартное нижнее бельё старшено инженера станции.

ent-ClothingUnderwearBottomBoxersCMO = боксеры ГВ
    .desc = Стандартное нижнее бельё главного врача станции. Почему-то они пахнут хлоралгидратом...

ent-ClothingUnderwearBottomBoxersHOP = боксеры ГП
    .desc = Стандартное нижнее бельё главы персонала станции.

ent-ClothingUnderwearBottomBoxersHOS = боксеры ГСБ
    .desc = Стандартное нижнее бельё главы службы безопасности станции.

ent-ClothingUnderwearBottomBoxersQM = боксеры КМ
    .desc = Стандартное нижнее бельё квартирмейстера станции.

ent-ClothingUnderwearBottomBoxersRD = боксеры НР
    .desc = Стандартное нижнее бельё научного руководителя станции.

ent-ClothingUnderwearBottomPantiesCap = трусики капитана
    .desc = Стандартное нижнее бельё капитана станции.

ent-ClothingUnderwearBottomPantiesCE = трусики СИ
    .desc = Стандартное нижнее бельё старшено инженера станции.

ent-ClothingUnderwearBottomPantiesCMO = трусики ГВ
    .desc = Стандартное нижнее бельё главного врача станции. Почему-то они пахнут хлоралгидратом...

ent-ClothingUnderwearBottomPantiesHOP = трусики ГП
    .desc = Стандартное нижнее бельё главы персонала станции.

ent-ClothingUnderwearBottomPantiesHOS = трусики ГСБ
    .desc = Стандартное нижнее бельё главы службы безопасности станции.

ent-ClothingUnderwearBottomPantiesQM = трусики КМ
    .desc = Стандартное нижнее бельё квартирмейстера станции.

ent-ClothingUnderwearBottomPantiesRD = трусики НР
    .desc = Стандартное нижнее бельё научного руководителя станции.

ent-ClothingUnderwearTopBraCap = бра капитана
    .desc = Стандартное нижнее бельё капитана станции.

ent-ClothingUnderwearTopBraCE = бра СИ
    .desc = Стандартное нижнее бельё старшено инженера станции.

ent-ClothingUnderwearTopBraCMO = бра ГВ
    .desc = Стандартное нижнее бельё главного врача станции. Почему-то оно пахнет хлоралгидратом...

ent-ClothingUnderwearTopBraHOP = бра ГП
    .desc = Стандартное нижнее бельё главы персонала станции.

ent-ClothingUnderwearTopBraHOS = бра ГСБ
    .desc = Стандартное нижнее бельё главы службы безопасности станции.

ent-ClothingUnderwearTopBraQM = бра КМ
    .desc = Стандартное нижнее бельё квартирмейстера станции.

ent-ClothingUnderwearTopBraRD = бра РД
    .desc = Стандартное нижнее бельё научного руководителя станции.
