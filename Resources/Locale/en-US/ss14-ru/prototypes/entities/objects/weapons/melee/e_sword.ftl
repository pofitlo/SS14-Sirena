ent-EnergySword = energy sword
    .desc = Very loud and very dangerous energy sword that can reflect shots. Can be stored in pockets when turned off.
    .suffix = { "" }
ent-EnergyDagger = pen
    .desc = A dark ink pen.
    .suffix = E-Dagger
